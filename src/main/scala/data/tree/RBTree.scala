package data.tree

import RBTree._
import scala.math.Ordering

/**
 * Incomplete attempt at a Red-Black tree in Scala. The tree is immutable;
 * each insert operation returns a new tree (and the original tree is intact).
 * I wrote this class as a simple exercise to reacquaint myself with Scala
 * and the Red-Black tree data structure. Note that the Scala library already
 * contains a general-purpose Red-Black tree implementation...
 */
case class RBTree[+A](root: Node[A] = Leaf) extends Traversable[A] {

  /**
   * Check if the tree contains a given value
   * @param aValue A value
   * @return True if the tree contains the value
   */
  def contains[B >: A](aValue: B)(implicit ord: Ordering[B]): Boolean =
    root.contains(aValue)

  /**
   * Insert a new value into the tree
   * @param newValue The new value
   * @return A copy of the tree with the new value inserted into it
   */
  def insert[B >: A](newValue: B)(implicit ord: Ordering[B]): RBTree[B] =
    RBTree(
      root.insert(newValue) match {
        case red@Tree(_, Red, _, _) => red.toBlack
        case node => node
      })

  def foreach[U](f: (A) => U) {
    root.foreach(f)
  }

}

object RBTree {

  type Color = Boolean
  val Black: Color = true
  val Red: Color = false

  def apply[T](values: Traversable[T])(implicit ord: Ordering[T]): RBTree[T] =
    values.foldLeft(RBTree[T]()) {
      (tree, value) => tree.insert(value)
    }

  def apply[T](value: T, values: T*)(implicit ord: Ordering[T]): RBTree[T] =
    values.foldLeft(RBTree().insert(value)) {
      (tree, value) => tree.insert(value)
    }


  /**
   * A node in a red-black tree. Can be colored red or black.
   */
  sealed trait Node[+A] {

    def contains[B >: A](aValue: B)(implicit ord: Ordering[B]): Boolean

    def insert[B >: A](newValue: B)(implicit ord: Ordering[B]): Node[B]

    def foreach[U](f: A => U)

  }

  /**
   * A leaf node (always colored black).
   */
  object Leaf extends Node[Nothing] {

    def contains[B >: Nothing](aValue: B)(implicit ord: Ordering[B]) = false

    def insert[B >: Nothing](newValue: B)(implicit ord: Ordering[B]): Node[B] =
      Tree[B](newValue, Red, Leaf, Leaf)

    def foreach[U](f: Nothing => U) {}

    override def toString = "Leaf"

  }

  /**
   * A tree node (with left and right children).
   *
   * @param value The node's value
   * @param color The node's color (red or black)
   * @param left The left branch
   * @param right The right branch
   */
  final case class Tree[+A](value: A, color: Color, left: Node[A] = Leaf, right: Node[A] = Leaf) extends Node[A] {

    import Ordered._

    def toBlack: Tree[A] =
      Tree(value, Black, left, right)

    def contains[B >: A](aValue: B)(implicit ord: Ordering[B]) = {
      if (aValue < value) left.contains(aValue)
      else if (aValue > value) right.contains(aValue)
      else true
    }

    def insert[B >: A](newValue: B)(implicit ord: Ordering[B]): Node[B] =
      if (newValue == value) this
      else if (newValue < value) {

        val parent = left.insert(newValue)
        val uncle = right

        parent match {
          case p@Tree(_, Red, _, n@Tree(_, Red, _, _)) =>
            uncle match {
              case u@Tree(_, Red, _, _) =>
                Tree(value, Red, p.toBlack, u.toBlack)
              case _ =>
                Tree(n.value, Black, p.copy(right = n.left), Tree(value, Red, n.right, right))
            }
          case p@Tree(_, Red, n@Tree(_, Red, _, _), _) =>
            uncle match {
              case u@Tree(_, Red, _, _) =>
                Tree(value, Red, p.toBlack, u.toBlack)
              case _ =>
                Tree(p.value, Black, n, Tree(value, Red, p.right, right))
            }
          case p =>
            Tree(value, color, parent, uncle)

        }


      } else {

        val uncle = left
        val parent = right.insert(newValue)

        parent match {
          case p@Tree(_, Red, n@Tree(_, Red, _, _), _) =>
            uncle match {
              case u@Tree(_, Red, _, _) =>
                Tree(value, Red, u.toBlack, p.toBlack)
              case _ =>
                Tree(n.value, Black, Tree(value, Red, uncle, n.left), p.copy(left = n.right))
            }
          case p@Tree(_, Red, _, n@Tree(_, Red, _, _)) =>
            uncle match {
              case u@Tree(_, Red, _, _) =>
                Tree(value, Red, u.toBlack, p.toBlack)
              case _ =>
                Tree(p.value, Black, Tree(value, Red, uncle, p.left), n)
            }
          case p =>
            Tree(value, color, uncle, parent)

        }

      }

    def foreach[U](f: A => U) {
      left.foreach(f)
      f(value)
      right.foreach(f)
    }

  }

}
