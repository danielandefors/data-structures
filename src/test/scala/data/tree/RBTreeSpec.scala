package data.tree

import org.scalatest.{Matchers, FlatSpec}
import RBTree._

/**
 *
 */
class RBTreeSpec extends FlatSpec with Matchers {

  "A RBTree" should "respect critical properties when empty" in {
    val tree = RBTree()
    tree.root should be(Leaf)
    tree.root.toString should be("Leaf")
  }

  it should "respect critical properties when inserting [1]" in {
    val tree = RBTree(1)
    tree.root should be(
      Tree(1, Black)
    )
  }

  it should "respect critical properties when inserting [1,2]" in {
    val tree = RBTree(1, 2)
    tree.root should be(
      Tree(1, Black,
        Leaf,
        Tree(2, Red)
      )
    )
  }

  it should "respect critical properties when inserting [2,1]" in {
    val tree = RBTree(2, 1)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Red),
        Leaf
      )
    )
  }

  it should "respect critical properties when inserting [1,2,3]" in {
    val tree = RBTree(1, 2, 3)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Red),
        Tree(3, Red)
      )
    )
  }

  it should "respect critical properties when inserting [1,3,2]" in {
    val tree = RBTree(1, 3, 2)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Red),
        Tree(3, Red)
      )
    )
  }

  it should "respect critical properties when inserting [2,1,3]" in {
    val tree = RBTree(2, 1, 3)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Red),
        Tree(3, Red)
      )
    )
  }

  it should "respect critical properties when inserting [2,3,1]" in {
    val tree = RBTree(2, 3, 1)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Red),
        Tree(3, Red)
      )
    )
  }

  it should "respect critical properties when inserting [3,2,1]" in {
    val tree = RBTree(3, 2, 1)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Red),
        Tree(3, Red)
      )
    )
  }

  it should "respect critical properties when inserting [3,1,2]" in {
    val tree = RBTree(3, 1, 2)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Red),
        Tree(3, Red)
      )
    )
  }

  it should "respect critical properties when inserting [1,2,3,4]" in {
    val tree = RBTree(1, 2, 3, 4)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Black),
        Tree(3, Black,
          Leaf,
          Tree(4, Red)
        )
      )
    )
  }

  it should "respect critical properties when inserting [2,4,1,3]" in {
    val tree = RBTree(2, 4, 1, 3)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Black),
        Tree(4, Black,
          Tree(3, Red),
          Leaf
        )
      )
    )
  }

  it should "respect critical properties when inserting [4,3,2,1]" in {
    val tree = RBTree(4, 3, 2, 1)
    tree.root should be(
      Tree(3, Black,
        Tree(2, Black,
          Tree(1, Red),
          Leaf
        ),
        Tree(4, Black)
      )
    )
  }

  it should "respect critical properties when inserting [1,2,3,4,5]" in {
    val tree = RBTree(1, 2, 3, 4, 5)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Black),
        Tree(4, Black,
          Tree(3, Red),
          Tree(5, Red)
        )
      )
    )
  }

  it should "respect critical properties when inserting [1,2,3,4,5,0]" in {
    val tree = RBTree(1, 2, 3, 4, 5, 0)
    tree.root should be(
      Tree(2, Black,
        Tree(1, Black,
          Tree(0, Red),
          Leaf
        ),
        Tree(4, Black,
          Tree(3, Red),
          Tree(5, Red)
        )
      )
    )
  }

  it should "contain the elements that have been inserted" in {
    val tree = RBTree(1, 2, 3)

    tree.contains(1) should be(true)
    tree.contains(2) should be(true)
    tree.contains(3) should be(true)

    tree.contains(4) should be(false)
    tree.contains(5) should be(false)
    tree.contains(0) should be(false)

  }

  it should "return a sorted list" in {
    val input = List(1, 5, 8, 12, 7, 2, 87, 3, 63, 61, 73)
    val tree = RBTree(input)
    tree.toList should be(input.sorted)
  }

  it should "accept strings too" in {
    val tree = RBTree("A", "B", "C")
    tree.root should be(
      Tree("B", Black,
        Tree("A", Red),
        Tree("C", Red)
      )
    )
  }

  it should "not add values that already exist" in {
    val tree = RBTree("A", "B", "C")
    tree.root should be(
      Tree("B", Black,
        Tree("A", Red),
        Tree("C", Red)
      )
    )
    val newTree = tree.insert("C")
    newTree.root should be(
      Tree("B", Black,
        Tree("A", Red),
        Tree("C", Red)
      )
    )
  }

}
