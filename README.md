# Data Structures

The Scala library contains many excellent data structures. The work in the 
repository are a collection of my own attempts at implementing various data 
structures in Scala. My excuse for duplicating the work of others is to revisit 
implementation details of common data structures and to familiarize myself with 
the Scala programming language. 

